import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;

public class SystemOps {

    // function + exception in case of failed file import
    public static Network makePlanetsFromFile(String filePath) throws FileNotFoundException
    {
        int nrOfPlanets; // = 0; doesn't need to be initialised since we parsed the entire file in lineArray
        double universeR; // = 0.0; doesn't need to be initialised since we parsed the entire file in lineArray
        Planet[] allPlanets; // declare Planet array allPlanets

        // file structure
        // file[0]: nrOfPlanets, file[1]: universeR, file[2:]: planets
        File planetsFile_open = new File(filePath);

        int nrOfLines = 0;
        Scanner planetsFile_scan1 = new Scanner(planetsFile_open); //creates a scanner instance with the lines of the file
        while (planetsFile_scan1.hasNextLine())
        {
            planetsFile_scan1.nextLine();
            nrOfLines += 1;
        }
        planetsFile_scan1.close();

        String[] lineArray; // declare
        lineArray = new String[nrOfLines]; // initialise

        int arrayIndex = 0;
        Scanner planetsFile_scan2 = new Scanner(planetsFile_open);
        while (planetsFile_scan2.hasNextLine())
        {
            String line = planetsFile_scan2.nextLine();
            if (line.isEmpty()) break;
            lineArray[arrayIndex] = line;
            arrayIndex += 1;
        }
        planetsFile_scan2.close(); //close to release the resource --> otherwise resource leak

        nrOfPlanets = parseInt(lineArray[0]);
        universeR = parseDouble(lineArray[1]);
        allPlanets = new Planet[nrOfPlanets]; // the allPlanets Planets-array will get [nrOfPlanets] Planet-instances
        // allPlanets --> from lineArray[2:]
        for (int i = 2; i < (nrOfPlanets+2); i++)
        {
            String planetLine = lineArray[i]; //
            String[] planetData = planetLine.split("\\s{2,}");
            Hector planetPos = new Hector(parseDouble(planetData[0]),
                                    parseDouble(planetData[1]));
            Hector planetVelo = new Hector(parseDouble(planetData[2]),
                                           parseDouble(planetData[3]));
            double planetMass = parseDouble(planetData[4]);
            String planetID = "planet"+(i-2);

            allPlanets[i-2] = new Planet(planetPos, planetVelo, planetMass, planetID);
        }
        Network planetarySystem = new Network(allPlanets, universeR);
        return planetarySystem;
    }
    public static void printSystem(Network network)
    {
        for (Planet eachPlanet : network.allPlanets)
        {
            eachPlanet.printPlanetInfo();
        }
    }

    public static void drawSystem(Network network)
    {
        StdDraw.enableDoubleBuffering();
        double R = network.universeR;
        StdDraw.setXscale(-R, R);
        StdDraw.setYscale(-R, R);
        StdDraw.clear(new Color(0,0,0, 20));
//        StdDraw.picture(0,0,"C:\\Users\\Meowy\\Codeventures\\planets\\resources\\starfield.jpg");
        for (int i = 0; i < network.nrOfPlanets; i++)
        {
            Planet eachPlanet = network.allPlanets[i];
            Color colour = MJcolours.basicColours[i];
            Color c = new Color(colour.getRed(), colour.getGreen(), colour.getBlue(), 255/5);
            StdDraw.setPenColor(c);
            double posX = eachPlanet.getPos().getX();
            double posY = eachPlanet.getPos().getY();
//            double radius = network.universeR/50;
//            StdDraw.filledCircle(posX,posY, radius);
            double size = Newtons.calculatePlanetSize(eachPlanet) * 70;
            double radius;
            if (size < network.universeR/20) radius = size;
            else radius = network.universeR/20;

            StdDraw.filledCircle(posX, posY, radius);
        }
        StdDraw.show();
        StdDraw.pause(20);
    }

    public static void updateSystem(Network exoSystem, double simSpeed)
    {
        double timeIncrement = simSpeed * 10e4;
        for (Planet eachPlanet : exoSystem.allPlanets)
        {
            Newtons.updateTotalPlanetAcc(eachPlanet, exoSystem.allPlanets, timeIncrement);
        }
        exoSystem.age += 1;
        System.out.println("System updated, age: " + exoSystem.age);
    }
}
