public class Network
{
    // attributes
    public Planet[] allPlanets;
    public double universeR;
    public int nrOfPlanets;
    public int age;

    // constructor
    public Network(Planet[] allPlanets, double universeR)
    {
        this.allPlanets = allPlanets;
        this.nrOfPlanets = allPlanets.length;
        this.universeR = universeR;
        this.age = 0;
    }
}
