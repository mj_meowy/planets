public class Planet
{
    private final double mass;
    private Hector pos; //position is an (x,y) tuple, i.e. represented as vector
    private Hector velo; //velocity is a vector
    private Hector acc; //acceleration is a vector
    public String planetID;
    //vector attributes and methods are stored in the Hector-class
    //applying force-generated acceleration (2D) to a moving object changes its velocity (2D)
    //change in velocity (2D) then results in change in direction of the movement
    //the method that's congruent with the class name is its constructor (smart)
    public Planet(Hector pos, Hector velo, double mass, String planetID) //constructor
    {
        this.pos = pos;
        this.velo = velo;
        this.mass = mass;
        this.acc = new Hector(0,0);
        this.planetID = planetID;
    }
    public void updateAcc(Hector freshAcc, double timeIncrement) //equivalent to directional change + distance travelled
    {
        //force informs acc --> acc changes velo (2D) --> 2D changes in velo define new position

        this.acc = freshAcc; //changes if forces on object have changed since last timeIncrement --> always Y in system!

        Hector velo_t = Hector.multiply(this.acc, timeIncrement); // velo changes proportionally with time
        this.velo = Hector.sum(this.velo, velo_t); // acceleration change (2D) translated to velocity

        Hector pos_t = Hector.multiply(this.velo, timeIncrement); // pos changes proportionally with time
        this.pos = Hector.sum(this.pos, pos_t); // velocity change (2D) translated to position

    }
    public Hector getPos()
    {
        return this.pos;
    }
    public Hector getVelo()
    {
        return this.velo;
    }
    public double getMass()
    {
        return this.mass;
    }

    public void printPlanetInfo()
    {
        String ID = this.planetID;
        String pos = this.getPos().toString();
        String velo = this.getVelo().toString();
        String mass = Double.toString(this.getMass());

        System.out.println(String.join("  ", ID, pos, velo, mass));
    }
    //
}
