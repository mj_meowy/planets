//Hectors have a direction (given by x, y) and can get additional Hectors added to them (addHector)
public class Hector //hector object class
{
    private double x; // has attributes x
    private double y; // and y

    //the method that's congruent with the class name is its constructor (smart)
    public Hector(double x, double y) // and a constructor to create a new Hector
    {
        this.x = x; //if you construct new hectors
        this.y = y; //your input will end up here
    }
    //other methods can do sh*t with the class
//    public void addHector(Hector additionalHector)
//    {
//        this.x += additionalHector.x;
//        this.y += additionalHector.y;
//    }
//    public void multiplyHector(int factor)
//    {
//        this.x *= factor;
//        this.y *= factor;
//    }
    public static Hector sum(Hector hector1, Hector hector2)
    {
        double x = hector1.getX() + hector2.getX();
        double y = hector1.getY() + hector2.getY();
        return new Hector(x, y);
    }
    public static Hector multiply(Hector hector, double factor)
    {
        double x = hector.getX() * factor;
        double y = hector.getY() * factor;
        return new Hector(x, y);
    }
    //good little java devs use get methods
    public double getX() { return this.x; }
    public double getY() { return this.y; }

    public String toString()
    {
        return (this.x + "/" + this.y);
    }
}
