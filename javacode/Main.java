//import java.io.File; //to handle File objects
import java.io.*; //all input/output libraries, i.e. File creation

import static java.lang.Integer.parseInt;


public class Main //the puppetmaster wrapper
{
    private static Network exoSystem; // declared array of Planet-instances
    public static void main(String[] args) throws IOException //the actual puppetmaster
    //static methods can be accessed without an instance of the class being present
    //non-static methods require an instance of the class tobe applied to
    {
        // original planets
        String path = "C:\\Users\\Meowy\\Codeventures\\planets\\resources\\planets.txt";

        // new exosystem
//        String path = "C:\\Users\\Meowy\\Codeventures\\planets\\resources\\planets2.txt";
        exoSystem = SystemOps.makePlanetsFromFile(path);
//        SystemOps.printSystem(exoSystem);
        int finalAge = 10000;

        for (int i = 0; i < finalAge; i++ )
        {
            SystemOps.drawSystem(exoSystem);
            SystemOps.updateSystem(exoSystem, 1.1); // simSpeed 1 = 10e5 timeIncrement
            // SystemOps.printSystem(exoSystem);

        }
        MJcolours.printColours();
    }
}
