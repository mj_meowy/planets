import java.awt.*;

public class MJcolours {

    // basic colours
    private static Color red1 = Color.decode("#ffca70");
    private static Color trq1 = Color.decode("#429a79");
    private static Color trq2 = Color.decode("#5ad1a5");
    private static Color trq3 = Color.decode("#85ffd2");
    private static Color trq4 = Color.decode("#a0fffc");

    public static Color[] basicColours = new Color[]{red,trq1,trq2,trq3,trq4};

    // rainbow
    private static Color red = Color.decode("#ffca70");
    private static Color orange = Color.decode("#429a79");
    private static Color yellow = Color.decode("#5ad1a5");
    private static Color green = Color.decode("#85ffd2");
    private static Color turq = Color.decode("#a0fffc");
    private static Color blue = Color.decode("#a0fffc");
    private static Color purple = Color.decode("#a0fffc");
    private static Color magenta = Color.decode("#a0fffc");


    public static void printColours()
    {
        for (Color colour : basicColours)
        {
          System.out.println(colour.getRGB());
        }
    }

}
