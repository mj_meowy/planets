import java.lang.Math;

public class Newtons
{
    public static double Gravitas = 6.67E-11;

    public static double interDistance(Planet planetA, Planet planetB)
    {
        return Math.sqrt(Math.pow((planetB.getPos().getX() - planetA.getPos().getX()),2) +
                         Math.pow((planetB.getPos().getY() - planetA.getPos().getY()),2));
        //a^2 + b^2 = c^2 --> c (r)
    }

    public static double interDistanceSqd(Planet planetA, Planet planetB)
    {
        return  Math.pow((planetB.getPos().getX() - planetA.getPos().getX()),2) +
                Math.pow((planetB.getPos().getY() - planetA.getPos().getY()),2);
        //a^2 + b^2 = c^2 --> c^2 (r^2)
    }

    public static double netForce(Planet planetA,Planet planetB)
    {
        double iDSqd = interDistanceSqd(planetA, planetB);
        double m1m2G = planetA.getMass() * planetB.getMass() * Gravitas;
        return m1m2G / iDSqd;
        //F = m1 * m2 * G * (r^2)^-1 --> F (net force)
    }

    private static Hector calculateForceXY(Planet planetCalculatePull,Planet planetPulling)
    {
        double nF = netForce(planetCalculatePull, planetPulling);
        double r = interDistance(planetCalculatePull, planetPulling);
        //separate force into x and y using sin & cos of angle between planets
        double dX = planetPulling.getPos().getX() - planetCalculatePull.getPos().getX(); //delta_x
        double dY = planetPulling.getPos().getY() - planetCalculatePull.getPos().getY(); //delta_y
        return new Hector(nF*(dX/r), nF*(dY/r)); //Fx = F * cosTH(-), Fy = F * sinTH(-) --> (Fx, Fy)
    }

    private static boolean isSamePlanet(Planet planetA, Planet planetB)
    {
        return planetA.planetID.equals(planetB.planetID);
    }

    public static void updateTotalPlanetAcc(Planet planetCalculatePull, Planet[] planetsPulling, double timeIncrement)
    {
        double pulledMass = planetCalculatePull.getMass();
        Hector trackNF = new Hector(0,0);
        for (Planet currentPlanet : planetsPulling) {
            if (isSamePlanet(planetCalculatePull, currentPlanet)) continue;
            Hector nextForce = calculateForceXY(planetCalculatePull, currentPlanet);
            trackNF = Hector.sum(trackNF, nextForce);
        }
        //System.out.println(planetCalculatePull.planetID+" updated");
        Hector netAcc = new Hector(trackNF.getX()/pulledMass, trackNF.getY()/pulledMass);
        planetCalculatePull.updateAcc(netAcc, timeIncrement);
    }

    public static double calculatePlanetSize(Planet planet)
    {
        double mass = planet.getMass();
        double r = Math.cbrt((3*mass) / (4 * Math.PI));
        return r;
    }



//    public static Hector calculateAccXY(Planet planetCalculatePull, Hector netForcePulling)
//    {
//        Hector forceXY = calculateForceXY(planetCalculatePull, planetPulling);
//        double pulledMass = planetCalculatePull.getMass();
//        return new Hector(forceXY.getX()/pulledMass, forceXY.getY()/pulledMass);
//    }
//
//    public static void netAcc(Planet planetCalculatePull, Planet[] planetsPulling)
//    {
//        Hector trackAcc = new Hector(0,0);
//        for (int i = 0; i < planetsPulling.length; i++)
//        {
//            if (isSamePlanet(planetCalculatePull, planetsPulling[i])) continue;
//            trackAcc.addHector(calculateAccXY(planetCalculatePull, planetsPulling[i]));
//            System.out.println(trackAcc);
//        }
//        planetCalculatePull.updateAcc(trackAcc);
//        System.out.println(planetCalculatePull.planetID+" updated");
//        return;
//    }
}
