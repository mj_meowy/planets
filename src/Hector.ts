//Hectors have a direction (given by x, y) and can get additional Hectors added to them (addHector)
class Hector //hector object class
{
    x: number; // has attributes x
    y: number; // and y

    //the method that's congruent with the class name is its constructor (smart)
    constructor(x: number, y: number) // and a constructor to create a new Hector
    {
        this.x = x; //if you construct new hectors
        this.y = y; //your input will end up here
    }

    
    //good little java devs use get methods
    // function on instance of class, doesn't need "const"
    getX = (): number => this.x;
    getY = (): number => this.y;
    
    toString = (): string =>
    {
        return (this.x + "/" + this.y);
    }
}

//other methods can do sh*t with the class
// define function 
const sum = (hector1: Hector, hector2: Hector): Hector =>
{
    const x: number = hector1.getX() + hector2.getX();
    const y: number = hector1.getY() + hector2.getY();
    return new Hector(x, y);
}

const multiply = (hector: Hector, factor: number) =>
{
    const x: number = hector.getX() * factor;
    const y: number = hector.getY() * factor;
    return new Hector(x, y);
}

// export to define as public
export 
{
    Hector,
    sum,
    multiply
}


