import { Hector, sum, multiply } from "./Hector";

class Planet
{
    mass: number;
    pos: Hector; //position is an (x,y) tuple, i.e. represented as vector
    velo: Hector; //velocity is a vector
    acc: Hector; //acceleration is a vector
    planetID: string;
    //vector attributes and methods are stored in the Hector-class
    //applying force-generated acceleration (2D) to a moving object changes its velocity (2D)
    //change in velocity (2D) then results in change in direction of the movement
    //the method that's congruent with the class name is its constructor (smart)
    constructor(pos: Hector, velo: Hector, mass: number, planetID: string) //constructor
    {
        this.pos = pos;
        this.velo = velo;
        this.mass = mass;
        this.acc = new Hector(0,0);
        this.planetID = planetID;
    }
    
    updateAcc = (freshAcc: Hector, timeIncrement: number) =>//equivalent to directional change + distance travelled
    {
        //force informs acc --> acc changes velo (2D) --> 2D changes in velo define new position

        this.acc = freshAcc; //changes if forces on object have changed since last timeIncrement --> always Y in system!

        const velo_t: Hector = multiply(this.acc, timeIncrement); // velo changes proportionally with time
        this.velo = sum(this.velo, velo_t); // acceleration change (2D) translated to velocity

        const pos_t: Hector = multiply(this.velo, timeIncrement); // pos changes proportionally with time
        this.pos = sum(this.pos, pos_t); // velocity change (2D) translated to position

    }

    getPos = (): Hector => this.pos;

    getVelo = (): Hector => this.velo;

    getMass = (): number => this.mass;

    printPlanetInfo = (): void =>
    {
        const ID: string = this.planetID;
        const pos: string = this.getPos().toString();
        const velo: string = this.getVelo().toString();
        const mass: string = this.getMass().toString();
        const info: string[] = [ID, pos, velo, mass]

        console.log(info.join("  "));
    }
}


